// MundoServidor.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////
// Autor David Andía

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "compartido.h"
#include "jugador.h"
#include "Esfera.h"
#include "Raqueta.h"
#include <pthread.h>
#include <string.h>

//incldue para el socket
#include "Socket.h"

class CMundoServidor
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();
	
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	mensaje m;
	jugador j1={"Jugador1", 0}, j2={"Jugador2", 0};
	int fd;			//identificador de tuberia
	pthread_t thid1;		//identificador de thread
	pthread_attr_t atrib;
	
	Socket conexion, comunicacion;
	char ipmaquina[100]="127.0.0.1";
	int puerto=9000;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
