#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include "DatosMemCompartida.h"

int main(){
	int fd;
	DatosMemCompartida *pdatoscomp;

	if ((fd=open("datos_BOT",O_RDWR)) < 0) {
        	perror("No se puede abrir");
		return(1);
        }
	pdatoscomp=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	if (pdatoscomp==MAP_FAILED){
		perror("No se puede proyectar");
		close(fd);
	}
	close(fd);

	while(pdatoscomp!=NULL){
		if(pdatoscomp->esfera.centro.y<pdatoscomp->raqueta1.y2)
			pdatoscomp->accion=-1;
		else if(pdatoscomp->esfera.centro.y>pdatoscomp->raqueta1.y1)
			pdatoscomp->accion=1;
		usleep(25000);
	}
	unlink("datos_BOT");
        return(0);
}
