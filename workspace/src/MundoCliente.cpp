
// MundoCliente.cpp: implementation of the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//includes para usar la tuberia y los ficheros
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//includes para el bot
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	/*close(fd_CS);
	unlink("FIFO_CS");
	close(fd_SC);
	unlink("FIFO_SC");*/
	close(fd_bot);
	munmap(datosComp, sizeof(DatosMemCompartida));
	unlink("datos_BOT");
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"%s: %d", j1.nombre, puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"%s: %d", j2.nombre, puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{
	char cad[200];
//enviar los datos por la tuberia CS

//recibir datos por la tuberia SC
	//read(fd_SC, &cad, sizeof(cad));
	comunicacion.Receive(cad, sizeof(cad));
		sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &esfera.radio, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 

//terminar el juego a los 3 puntos
	if(puntos1==3){
		printf("Victoria para %s", j1.nombre);
		exit(1);
	}
	else if(puntos2==3){
		printf("Victoria para %s", j2.nombre);
		exit(1);
	}

//pasar datos al bot
	datosComp->esfera=esfera;
	datosComp->raqueta1=jugador2;
//funcionamiento del bot
	if(datosComp->accion==1) OnKeyboardDown('o',0,0);
	else if(datosComp->accion==-1) OnKeyboardDown('l',0,0);
	else if(datosComp->accion==0) OnKeyboardDown(' ',0,0);

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
/*	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case ' ':jugador1.velocidad.y=0;
		jugador2.velocidad.y=0;break;
	
	}*/
	char cad[]="0";
	sprintf(cad, "%c", key);
	//write(fd_CS, &cad, sizeof(cad));
	comunicacion.Send(cad, sizeof(cad));
}

void CMundoCliente::Init()
{
//inicializacion necesaria para el bot
	if((fd_bot=open("datos_BOT", O_CREAT|O_TRUNC|O_RDWR, 0666))<0){
		perror("No se pueden crear datos");
	}
	datosCompartidos.esfera=esfera;
	datosCompartidos.raqueta1=jugador2;
	datosCompartidos.accion=0;
	write(fd_bot, &datosCompartidos, sizeof(datosCompartidos));
	datosComp=static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(datosCompartidos), PROT_READ|PROT_WRITE, MAP_SHARED, fd_bot, 0));
	close(fd_bot);
	if(datosComp==MAP_FAILED){
		perror("No se puede proyectar en memoria");
	}


// borra FIFOs por si existían previamente
/*	unlink("FIFO_CS");
	unlink("FIFO_SC");*/
    	
/* Crea FIFOs */
/*	mkfifo("FIFO_SC", 0600);
	fd_SC=open("FIFO_SC",O_RDONLY);
	
	mkfifo("FIFO_CS", 0600);
	fd_CS=open("FIFO_CS",O_WRONLY);*/

//introducir nombre
	printf("Introduzca nombre del jugador:\n");
	scanf("%s", j1.nombre);
	comunicacion.Connect(ipmaquina, puerto);
	comunicacion.Send(j1.nombre, sizeof(j1.nombre));


	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
