# Changelog
Todos los cambios se escribirán en este archivo.

## Práctica 4

## v4.1
- Se ha finalizado la práctica.

### Añadido
- Programa Cliente.
- Programa Servidor.
- Comunicación con FIFOs.
- Thread del Servidor.


## Práctica 3

## v3.1
- Se ha finalizado la práctica

### Añadido
- Programa logger.
- Programa bot.
- Comunicación logger-tenis y bot-tenis.
- Fin del juego al alcanzar 3 puntos.


## Práctica 2

## v2.1
- Se ha finalizado la práctica.

### Añadido
- Movimiento de las raquetas.
- Movimiento de la esfera.
- Cambio de la esfera por esfera pulsante.

## Práctica 1

## v1.2
- Se ha finalizado la práctica.

## v1.1
### Añadido
- El nombre del autor.
- Este archivo.
